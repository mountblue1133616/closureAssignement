let cacheFunction = require(`../cacheFunction.cjs`);
// let cb = [1];
let cb = function (num) {
  console.log("First Time");
  return num * num;
};
try {
  let res = cacheFunction(cb);

  console.log(res(4));
  console.log(res(3));
  console.log(res(5));
  console.log(res(4));
  console.log(res("ae"));
} catch (e) {
  console.error(e.message);
}
