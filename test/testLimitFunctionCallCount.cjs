const limitFunctionCallCount = require(`../limitFunctionCallCount.cjs`);
const cb = (x) => {
  return x * x;
};
try {
  const res = limitFunctionCallCount(cb, 1);
  console.log(res(5));
  console.log(res(5));
  console.log(res(5));
  console.log(res(5));
} catch (e) {
  console.error(e.message);
}
