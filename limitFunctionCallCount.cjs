// Should return a function that invokes `cb`.
// The returned function should only allow `cb` to be invoked `n` times.
// Returning null is acceptable if cb can't be returned
function limitFunctionCallCount(cb, n) {
  if (typeof cb !== "function" || n < 0 || typeof n !== "number") {
    throw new Error("Datatype incorrect!");
  }
  let count = 0;
  return function (...val) {
    if (count < n) {
      count++;
      return cb(...val);
    } else return null;
  };
}

module.exports = limitFunctionCallCount;
