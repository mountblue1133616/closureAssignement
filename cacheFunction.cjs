function cacheFunction(cb) {
  if (typeof cb !== "function") {
    throw new Error("CallBack is not a valid function!");
  }
  const cache = {};
  return function (...val) {
    if (!cache.hasOwnProperty(val)) {
      let res = cb(...val);
      cache[val] = res;
    }
    return cache[val];
  };
}

module.exports = cacheFunction;
